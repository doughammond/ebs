import datetime
import random
import numpy
from dateutil import rrule
import math
import matplotlib.pyplot as plt
import matplotlib.dates


def calc_work_end(startDate, addWorkDays):
	""" Calculate the working end-date when adding work time to the starting work time
	"""

	# Create a rule to recur every weekday starting today
	workingHours = [9,10,11,12,14,15,16,17]
	r = rrule.rrule(rrule.HOURLY,
					byweekday=[rrule.MO, rrule.TU, rrule.WE, rrule.TH, rrule.FR],
					byhour=workingHours,
					byminute=range(60),
					bysecond=[0],
					dtstart=startDate)

	# Create a rruleset
	rs = rrule.rruleset()
	# Attach our rrule to it
	rs.rrule(r)
	
	addSeconds = int(addWorkDays * len(workingHours) * 60)
	return rs[addSeconds]

def do_mc_iter(velocities, estimates):
	""" Perform one iteration of monte-carlo estimation;
		This will return a new set of estimates, each one multiplied by
		a velocity randomly chosen from the velocities list.
	"""

	betterEstimates = [
		est * random.choice(velocities)
			for est in estimates
	]

	return betterEstimates

def make_percentile(estimate_sets, i, p):
	""" For a slice i in estimate sets, calculate the pth percentile
	"""
	return numpy.percentile([es[i] for es in estimate_sets], p)

def mcEstimateCompletion(velocities, estimates, startDate, iterations=100, percentiles=None):

	percentiles = percentiles or range(1, 101)

	# a list of MC adjusted estimates
	mc_estimate_sets = [
		do_mc_iter(velocities, estimates)
			for _ in xrange(iterations)
	]

	# for each task in the estimates list, calculate all of the percentiles
	mc_percentiles = [
		[make_percentile(mc_estimate_sets, i, p) for p in percentiles]
			for i in xrange(len(estimates))
	]

	# sum up all of the MC estimates for each percentile value
	mc_totals = [
		sum(pc[i] for pc in mc_percentiles)
			for i in xrange(len(percentiles))
	]

	# calculate the working-time end for each percentile value
	mc_ends = [
		calc_work_end(startDate, mc_total)
			for mc_total in mc_totals
	]

	return mc_ends, mc_totals


if __name__ == '__main__':
	# estimate/actual for past tasks completed
	myVelocities = [0.5, 1, 2, 0.75, 0.45, 0.85, 1]

	# best guess estimates for next tasks, in order
	nextTaskEstimates = [1, 0.5, 0.5, 2, 1, 1.5]

	# when the next set of tasks will start
	startDate = datetime.datetime(2016, 2, 15, 9, 0)

	perfectDeliveryDate = calc_work_end(startDate, sum(nextTaskEstimates))
	print 'In a perfect world, %d tasks will start on %s and end on %s (%0.2f)' % (
		len(nextTaskEstimates),
		startDate,
		perfectDeliveryDate,
		sum(nextTaskEstimates)
	)

	# print out a few percentile end dates
	percentiles = [5, 50, 80, 95]
	mc_ends, mc_totals = mcEstimateCompletion(myVelocities, nextTaskEstimates, startDate, percentiles=percentiles)
	for i, pc in enumerate(percentiles):
		print('% 3s percentile ends on %s (%0.2f)' % (
			pc,
			mc_ends[i],
			mc_totals[i]
		))

	# trying to plot out best-case and worst-case schedules... :/
	# fig, ax = plt.subplots()
	# j = 0
	# for i in xrange(1, len(nextTaskEstimates) + 1):
	# 	estimateSlice = nextTaskEstimates[:i]
	# 	startEstimateSlice = sum(nextTaskEstimates[:i-1])
	# 	sumEstimateSlice = sum(estimateSlice)
	# 	currentTask = nextTaskEstimates[i-1]
	# 	pc5, pc80 = mcEstimateCompletion(myVelocities, estimateSlice, startDate, percentiles=[5, 80])
	# 	# print(sumEstimateSlice, currentTask, estimateSlice)
	# 	ctStart = matplotlib.dates.date2num(calc_work_end(startDate, startEstimateSlice))
	# 	ctEnd  = calc_work_end(startDate, startEstimateSlice + currentTask)
	# 	for endVal in [pc5, ctEnd, pc80]:
	# 		endVal = matplotlib.dates.date2num(endVal)
	# 		ax.barh(-j, endVal - ctStart, left=ctStart)
	# 		j += 1
	# ax.xaxis_date()
	# fig.autofmt_xdate()
	# plt.show()

